import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from src.defi import Defi

class TestNavegacao(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://www.saucedemo.com/")
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_PADRAO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()

    def tearDown(self):
        self.driver.quit()

    def test_CT301_adicionar_produto_tela_produto(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        time.sleep(4)
        self.assertEqual('1', self.driver.find_element(by=By.CLASS_NAME, value="shopping_cart_badge").text)


    def test_CT302_adicionar_produto_tela_produto_especifico(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='item_4_title_link']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        time.sleep(4)
        self.assertEqual('1', self.driver.find_element(by=By.CLASS_NAME, value="shopping_cart_badge").text)

    def test_CT303_remover_produto_tela_produto(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='remove-sauce-labs-backpack']").click()
        self.assertEqual('', self.driver.find_element(by=By.CLASS_NAME, value="shopping_cart_container").text)


    def test_CT304_remover_produto_tela_produto_especifico(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='item_4_title_link']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='remove-sauce-labs-backpack']").click()
        self.assertEqual('', self.driver.find_element(by=By.CLASS_NAME, value="shopping_cart_container").text)

    def test_CT305_remover_produto_tela_carrinho2(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='remove-sauce-labs-backpack']").click()
        self.assertEqual('', self.driver.find_element(by=By.CLASS_NAME, value="shopping_cart_container").text)

    def test_CT307_checkout_com_produto(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='checkout']").click()
        self.assertEqual(Defi.URL_PASSO_UM,self.driver.current_url)

    def test_CT308_checkout_sem_produto(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='checkout']").click()
        self.assertEqual(Defi.URL_CARRINHO,self.driver.current_url)

    def test_CT309_compra_sem_nome(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='checkout']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='continue']").click()
        self.assertEqual(Defi.COMPRA_SEM_NOME, self.driver.find_element(by=By.XPATH, value="//*[@id='checkout_info_container']").text)

    def test_CT310_compra_sem_sobrenome(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='checkout']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='first-name']").send_keys(Defi.COMPRA_NOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='continue']").click()
        self.assertEqual(Defi.COMPRA_SEM_SOBRENOME, self.driver.find_element(by=By.XPATH, value="//*[@id='checkout_info_container']").text)

    def test_CT311_compra_sem_cep(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='checkout']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='first-name']").send_keys(Defi.COMPRA_NOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='last-name']").send_keys(Defi.COMPRA_SOBRENOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='continue']").click()
        self.assertEqual(Defi.COMPRA_SEM_CEP, self.driver.find_element(by=By.XPATH, value="//*[@id='checkout_info_container']").text)

    def test_CT312_compra_todas_informacoes(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='checkout']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='first-name']").send_keys(Defi.COMPRA_NOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='last-name']").send_keys(Defi.COMPRA_SOBRENOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='postal-code']").send_keys(Defi.COMPRA_SOBRENOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='continue']").click()
        self.assertEqual(Defi.URL_PASSO_DOIS,self.driver.current_url)

    def test_CT313_compra_cancelar(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='checkout']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='first-name']").send_keys(Defi.COMPRA_NOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='last-name']").send_keys(Defi.COMPRA_SOBRENOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='postal-code']").send_keys(Defi.COMPRA_SOBRENOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='continue']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='cancel']").click()
        self.assertEqual(Defi.URl_PRODUTO,self.driver.current_url)

    def test_CT314_compra_finalizada(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='checkout']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='first-name']").send_keys(Defi.COMPRA_NOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='last-name']").send_keys(Defi.COMPRA_SOBRENOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='postal-code']").send_keys(Defi.COMPRA_SOBRENOME)
        self.driver.find_element(by=By.XPATH, value="//*[@id='continue']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='finish']").click()
        self.assertEqual(Defi.URL_COMPRA_REALIZADA,self.driver.current_url)



    if __name__ == '__main__':
        unittest.main()