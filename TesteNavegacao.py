import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from src.defi import Defi

class TestNavegacao(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://www.saucedemo.com/")
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_PADRAO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()

    def tearDown(self):
        self.driver.quit()

    def test_CT201_ordenacao_alfabetica(self):
        filtro = self.driver.find_element(by=By.CLASS_NAME, value='product_sort_container')
        filtro.click()
        filtro.send_keys(Defi.Menu_Z_A)
        time.sleep(1)
        filtro.send_keys(Defi.Menu_A_Z)
        filtro.click()
        self.assertEqual(Defi.Menu_A_Z, self.driver.find_element(by=By.CLASS_NAME, value='active_option').text)

    def test_CT202_ordenacao_z_a(self):
        filtro = self.driver.find_element(by=By.CLASS_NAME, value='product_sort_container')
        filtro.click()
        filtro.send_keys(Defi.Menu_A_Z)
        time.sleep(1)
        filtro.send_keys(Defi.Menu_Z_A)
        filtro.click()
        self.assertEqual(Defi.Menu_Z_A, self.driver.find_element(by=By.CLASS_NAME, value='active_option').text)

    def test_CT203_ordenacao_crecente(self):
        filtro = self.driver.find_element(by=By.CLASS_NAME, value='product_sort_container')
        filtro.click()
        filtro.send_keys(Defi.Menu_Decrescente)
        time.sleep(1)
        filtro.send_keys(Defi.Menu_Crescente)
        filtro.click()
        self.assertEqual(Defi.Menu_Crescente, self.driver.find_element(by=By.CLASS_NAME, value='active_option').text)

    def test_CT204_ordenacao_decrecente(self):
        filtro = self.driver.find_element(by=By.CLASS_NAME, value='product_sort_container')
        filtro.click()
        filtro.send_keys(Defi.Menu_Crescente)
        time.sleep(1)
        filtro.send_keys(Defi.Menu_Decrescente)
        filtro.click()
        self.assertEqual(Defi.Menu_Decrescente, self.driver.find_element(by=By.CLASS_NAME, value='active_option').text)

    def test_CT205_menu_allitens(self):
        self.driver.find_element(by=By.CLASS_NAME, value='bm-burger-button').click()
        self.driver.find_element(by=By.ID, value='inventory_sidebar_link').click()
        time.sleep(2)
        self.assertEqual(Defi.PAG_PRODUTO, self.driver.find_element(by=By.CLASS_NAME, value="title").text)

    def test_CT206_menu_about(self):
        self.driver.find_element(by=By.CLASS_NAME, value='bm-burger-button').click()
        self.driver.find_element(by=By.ID, value='about_sidebar_link').click()
        time.sleep(1)
        self.assertEqual(Defi.URL_ABOUT, self.driver.current_url)


    def test_CT208_menu_resetapp(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='add-to-cart-sauce-labs-backpack']").click()
        self.driver.find_element(by=By.CLASS_NAME, value='bm-burger-button').click()
        self.driver.find_element(by=By.ID, value='reset_sidebar_link').click()
        time.sleep(1)
        self.assertEqual('', self.driver.find_element(by=By.CLASS_NAME, value="shopping_cart_container").text)

    def test_CT209_selecionar_produto(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='item_4_title_link']").click()
        time.sleep(1)
        self.assertEqual(Defi.PAG_PRODUTO_DESCRICAO, self.driver.find_element(by=By.CLASS_NAME, value="left_component").text)

    def test_CT210_selecionar_produto(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='item_4_title_link']").click()
        self.driver.find_element(by=By.CLASS_NAME, value="left_component").click()
        self.assertEqual(Defi.PAG_PRODUTO, self.driver.find_element(by=By.CLASS_NAME, value="title").text)

    def test_CT211_acessar_carrinho(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.assertEqual(Defi.URL_CARRINHO, self.driver.current_url)

    def test_CT212_voltar_carrinho(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id='shopping_cart_container']").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id='continue-shopping']").click()
        self.assertEqual(Defi.PAG_PRODUTO, self.driver.find_element(by=By.CLASS_NAME, value="title").text)


    if __name__ == '__main__':
        unittest.main()