import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from src.defi import Defi

class TestLoginLogout(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://www.saucedemo.com/")

    def tearDown(self):
        self.driver.quit()

    def test_CT101_login_usuario_valido_senha_valida(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_PADRAO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.PAG_PRODUTO, self.driver.find_element(by=By.CLASS_NAME, value="title").text)

    def test_CT102_login_invalido_senha_valida(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_INVALIDO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.LOGIN_INVALIDO, self.driver.find_element(by=By.CLASS_NAME, value='error-message-container').text)

    def test_CT103_login_branco_senha_valida(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_BRANCO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.LOGIN_USUARIO_BRANCO, self.driver.find_element(by=By.CLASS_NAME, value='error-message-container').text)

    def test_CT104_login_valido_senha_invalida(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_PADRAO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_INVALIDA)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.LOGIN_INVALIDO, self.driver.find_element(by=By.CLASS_NAME, value='error-message-container').text)

    def test_CT105_login_valido_senha_branco(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_PADRAO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_BRANCO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.LOGIN_SENHA_BRANCO,self.driver.find_element(by=By.CLASS_NAME, value='error-message-container').text)

    def test_CT106_login_valido_senha_branco(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_BRANCO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_BRANCO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.LOGIN_USUARIO_BRANCO,
                         self.driver.find_element(by=By.CLASS_NAME, value='error-message-container').text)

    def test_CT107_login_bloqueado(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_BLOQUEADO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.LOGIN_BLOQUEADO,
                         self.driver.find_element(by=By.CLASS_NAME, value='error-message-container').text)

    def test_CT108_login_problema(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_PROBLEMA)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.PAG_PRODUTO, self.driver.find_element(by=By.CLASS_NAME, value="title").text)

    def test_CT109_login_problema(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_BAIXODESEMPENHO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.PAG_PRODUTO, self.driver.find_element(by=By.CLASS_NAME, value="title").text)

    def test_CT110_login_erro(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_ERRO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.PAG_PRODUTO, self.driver.find_element(by=By.CLASS_NAME, value="title").text)

    def test_CT111_login_visual_erro(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_VISUAL_ERRO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.assertEqual(Defi.PAG_PRODUTO, self.driver.find_element(by=By.CLASS_NAME, value="title").text)

    def test_CT112_logout(self):
        self.driver.find_element(by=By.ID, value='user-name').send_keys(Defi.USUARIO_PADRAO)
        self.driver.find_element(by=By.ID, value='password').send_keys(Defi.SENHA_PADRAO)
        self.driver.find_element(by=By.ID, value='login-button').click()
        self.driver.find_element(by=By.CLASS_NAME, value='bm-burger-button').click()
        self.driver.find_element(by=By.ID, value='logout_sidebar_link').click()
        time.sleep(2)
        self.assertEqual(Defi.URL_LOGIN, self.driver.current_url)


if __name__ == '__main__':
    unittest.main()
