"""Define as Lables que serão usadas nos tests"""


class Defi:

    USUARIO_PADRAO = "standard_user"
    USUARIO_INVALIDO = "invalid_user"
    USUARIO_BRANCO = ""
    USUARIO_BLOQUEADO= "locked_out_user"
    USUARIO_PROBLEMA = "problem_user"
    USUARIO_BAIXODESEMPENHO = "performance_glitch_user"
    USUARIO_ERRO= "error_user"
    USUARIO_VISUAL_ERRO = "visual_user"

    SENHA_PADRAO = "secret_sauce"
    SENHA_BRANCO = ""
    SENHA_INVALIDA = "abcd"

    LOGIN_INVALIDO = "Epic sadface: Username and password do not match any user in this service"
    LOGIN_USUARIO_BRANCO = "Epic sadface: Username is required"
    LOGIN_SENHA_BRANCO = "Epic sadface: Password is required"
    LOGIN_BLOQUEADO = "Epic sadface: Sorry, this user has been locked out."


    PAG_PRODUTO = "Products"
    PAG_PRODUTO_DESCRICAO = "Back to products"
    URL_ABOUT = "https://saucelabs.com/"
    URL_LOGIN= "https://www.saucedemo.com/"
    URl_PRODUTO = "https://www.saucedemo.com/inventory.html"
    URL_CARRINHO = "https://www.saucedemo.com/cart.html"
    URL_PASSO_UM = "https://www.saucedemo.com/checkout-step-one.html"
    URL_PASSO_DOIS = "https://www.saucedemo.com/checkout-step-two.html"
    URL_COMPRA_REALIZADA = "https://www.saucedemo.com/checkout-complete.html"

    Menu_A_Z = "Name (A to Z)"
    Menu_Z_A = "Name (Z to A)"
    Menu_Crescente = "Price (low to high)"
    Menu_Decrescente = "Price (high to low)"


    COMPRA_SEM_NOME = "Error: First Name is required\nCancel"
    COMPRA_SEM_SOBRENOME = "Error: Last Name is required\nCancel"
    COMPRA_SEM_CEP= "Error: Postal Code is required\nCancel"
    COMPRA_NOME = "GUILHERME"
    COMPRA_SOBRENOME = "RODAMILANS"
    COMPRA_CEP = 0000000000







    STANDARD_USER = "standard_user"
    LOCKED_OUT_USER = "locked_out_user"
    PROBLEM_USER = "problem_user"
    PERFORMANCE_GLITCH_USER = "performance_glitch_user"
    ERROR_USER = "error_user"
    VISUAL_USER = "visual_user"

    MAIN_PAGE_PASSWORD_ALL_USERS = "Password for all users:\nsecret_sauce"

    INVENTORY_TITLE = "Products"
    CART_PAGE_TITLE = "Your Cart"
    CHECKOUT_PAGE_TITLE = "Checkout: Your Information"
    CHECKOUT_PAGE_OVERVIEW = "Checkout: Overview"
    CHECKOUT_PAGE_COMPLETE = "Checkout: Complete!"
    CHECKOUT_FIRTNAME_REQUERID = "Error: First Name is required"
    CHECKOUT_LASTNAME_REQUERID = "Error: Last Name is required"
    CHECKOUT_ZIPCODE_REQUERID = "Error: Postal Code is required"
    CHECKOUT_FIRTNAME_TEXT = "a"
    CHECKOUT_LASTNAME_TEXT = "a"
    CHECKOUT_ZIPCODE_TEXT = "a"
    CHECKOUT_FIRTNAME_EMPTY = ""
    CHECKOUT_LASTNAME_EMPTY = ""
    CHECKOUT_ZIPCODE_EMPTY = ""
    SORT_Z_TO_A = "Name (Z to A)"
    SORT_A_TO_Z = "Name (A to Z)"
    SORT_PRICE_LOW_TO_HIGH = "Price (low to high)"
    SORT_PRICE_HIGH_TO_LOW = "Price (high to low)"
    CART_ADD_REMOVE_SAUCE_LABS_BACKPACK_TEXT = "Sauce Labs Backpack"
    CART_ADD_REMOVE_SAUCE_LABS_BACKPACK_PRICE = "29.99"
    CART_ADD_QTD_1 = '1'
    CART_EMPTY_QTD = ''